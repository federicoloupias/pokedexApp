import React, { useEffect, useRef, useState } from 'react'
import ImageColors from 'react-native-image-colors';
import { Dimensions, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { SimplePokemon } from '../interfaces/pokemonInterfaces';
import { FadeInImage } from './FadeInImage';
import { useNavigation } from '@react-navigation/native';
import { RootStackParams } from '../navigator/Navigator';
import { StackNavigationProp } from '@react-navigation/stack';

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height


interface Props {
    pokemon: SimplePokemon
}



export const PokemonCard = ({ pokemon }: Props) => {

    const [bgColor, setBgColor] = useState('grey');
    const isMounted = useRef(true);

    const navigator = useNavigation<StackNavigationProp<RootStackParams>>();


    useEffect(() => {
        const fetchColors = async () => {
            const url = pokemon.picture;
            const result = await ImageColors.getColors(url, {
                fallback: 'grey',
                cache: true,
                key: url,
            })

            if (!isMounted.current) return;
            switch (result.platform) {
                case 'android':
                    setBgColor(result.dominant || bgColor);
                break
                case 'ios':
                    setBgColor(result.background || bgColor);
                break
                default:
                setBgColor(bgColor);
                throw new Error('Unexpected platform')
            }
        }
        fetchColors();

        return () => {
            isMounted.current = false;
        }

    }, [])
      


  return (
    <TouchableOpacity
        onPress={() => {
            navigator.navigate('PokemonScreen', { simplePokemon: pokemon, color: bgColor});
        }}
        activeOpacity={0.9}
    >

        <View style={{
            ...styles.cardContainer,
            width: windowWidth *0.4,
            backgroundColor: bgColor
        }}>
            {/* Nombre del Pokemon y ID */}
            <View>
                <Text style={{
                    ...styles.name,
                }}> 
                { pokemon.name } 
                { '\n#' + pokemon.id }
                </Text>
            </View>

            <View
                style={
                    styles.pokebolaContainer
                }
            >
                <Image
                    source={require('../assets/pokebola-blanca.png')}
                    style={
                        styles.pokebola
                    }
                />
            </View>

            
            <FadeInImage 
                uri={ pokemon.picture }
                style={styles.pokemonImage}
            />

        </View>
        


    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
    cardContainer: {
        marginHorizontal: 10,
        // backgroundColor: 'red',
        height: 120,
        width: 160,
        marginBottom: 25,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    name: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        top:20,
        left: 10
    },
    pokebola: {
        position:'absolute',
        width: 100,
        height: 100,
        bottom: -25,
        right: -25,
        opacity:0.5
    },
    pokemonImage: {
        height:100,
        width:100,
        position:'absolute',
        bottom: -5,
        right: -8,
    },
    pokebolaContainer: {
        width: 100,
        height:100,
        position:'absolute',
        overflow:'hidden',
        right:0,
        bottom: 0,

    }
})
