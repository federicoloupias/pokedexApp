import React from 'react'
import { Text, View, StyleSheet } from 'react-native';
import { PokemonFull } from '../interfaces/pokemonInterfaces'
import { ScrollView } from 'react-native-gesture-handler'
import { FadeInImage } from './FadeInImage';

interface Props {
    pokemon: PokemonFull
}

export const PokemonDetails = ({ pokemon }: Props) => {
  return (
    <ScrollView
        showsVerticalScrollIndicator={false}
        style={{
            ...StyleSheet.absoluteFillObject,
        }}
    >
        {/* Types y Peso */}
        <View
            style= {{
                ...styles.container,
                marginTop: 370
            }}
        >
            <Text style={styles.title}>Types</Text>
            <View
                style={{
                    flexDirection: 'row'
                }}
            >
                {
                    pokemon.types.map(({ type }) => (
                        <Text
                            style= {{
                                color:'black',
                                marginRight: 10
                            }}
                            key={type.name}
                        >
                            {type.name}
                        </Text>
                    ))
                }
            </View>
            
            {/* Peso */}
            <Text style={styles.title}>Peso</Text>
            <Text style={styles.regularText}>{ pokemon.weight }kg</Text>


        </View>

        {/* Sprites */}
        <View style= { styles.container }>
            <Text style={styles.title}>Sprites</Text>
        </View>

        <ScrollView
            // style: {

            // }
            horizontal= {true}
            showsHorizontalScrollIndicator={false}

        >
            <FadeInImage 
                style={ styles.basicSprite }
                uri={ pokemon.sprites.front_default }
            />

            <FadeInImage 
                style={ styles.basicSprite }
                uri={ pokemon.sprites.back_default }
            />

            <FadeInImage 
                style={ styles.basicSprite }
                uri={ pokemon.sprites.front_shiny }
            />
            <FadeInImage 
                style={ styles.basicSprite }
                uri={ pokemon.sprites.back_shiny }
            />
            
        </ScrollView>

        {/* Habilidades */}
        <View
            style= {{
                ...styles.container,
            }}
        >
            <Text style={styles.title}>Base skills</Text>
        <View
                style={{
                    flexDirection: 'row'
                }}
            >
                {
                    pokemon.abilities.map(({ ability }) => (
                        <Text
                            style= {{
                                ...styles.regularText,
                                marginRight: 10
                            }}
                            key={ability.name}
                        >
                            {ability.name}
                        </Text>
                    ))
                }
            </View>
        </View>
        

        {/* Movimiento */}
        <View
            style= {{
                ...styles.container,
            }}
        >
            <Text style={styles.title}>Movement</Text>
        <View
                style={{
                    flexDirection: 'row',
                    flexWrap: 'wrap'
                }}
            >
                {
                    pokemon.moves.map(({ move }) => (
                        <Text
                            style= {{
                                ...styles.regularText,
                                marginRight: 10
                            }}
                            key={move.name}
                        >
                            {move.name}
                        </Text>
                    ))
                }
            </View>
        </View>

        {/* Stats */}
        <View
            style= {{
                ...styles.container,
            }}
        >
            <Text style={styles.title}>Stats</Text>
        <View>
                {
                    pokemon.stats.map(( stat, i ) => (
                        <View
                            key={ stat.stat.name + i }
                            style={{ 
                                flexDirection: 'row'
                            }}
                        >
                            <Text
                                style= {{
                                    ...styles.regularText,
                                    marginRight: 10,
                                    width: 150
                                }}
                            >
                                {stat.stat.name}
                            </Text>

                            <Text
                                style= {{
                                    ...styles.regularText,
                                    fontWeight: 'bold'
                                }}
                            >
                                {stat.base_stat}
                            </Text>
                        </View>
                    ))
                }
            </View>

                {/* Sprite Final */}
            <View style={{
                marginBottom: 20,
                alignItems: 'center'
            }}>
                <FadeInImage 
                    style={ styles.basicSprite }
                    uri={ pokemon.sprites.front_shiny }
                />
            </View>


        </View>
        

    </ScrollView>
  )
}


const styles = StyleSheet.create({
    container: {
        marginHorizontal: 20
    },
    title: {
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 20
    },
    regularText: {
        color: 'black',
    },
    basicSprite: {
        height:100,
        width:100
    }
})