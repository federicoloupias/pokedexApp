import { useEffect, useState } from 'react'
import { pokemonApi } from '../api/pokemonApi';
import { PokemonFull } from "../interfaces/pokemonInterfaces";


export const usePokemon = (id: string) => {
    const [isLoading, setIsLoading] = useState(true)
	const [pokemon, setPokemon] = useState<PokemonFull>({} as PokemonFull);
    const url = `https://pokeapi.co/api/v2/pokemon/${ id }`
    console.log('url - ', url);   


	const loadPokemon = async() => {
        try {
            const res = await pokemonApi.get<PokemonFull>(url);
            setPokemon(res.data);
        } catch (error) {
            return error;
        } finally {
            setIsLoading(false);
        }
	}
    
    useEffect(() => {
        loadPokemon();
    }, [])
    

	return {
		isLoading,
        pokemon,
	}
}
